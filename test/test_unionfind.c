#include "unionfind.h"
#include <stdio.h>

#define MSIZE   (3)
int main(void)
{
    int M[MSIZE][MSIZE] = {
        {1, 1, 0},
        {1, 1, 0},
        {0, 0, 1},
    };
    UnionFind *uf;

    uf = UFInit(MSIZE);
    for (int i = 0; i < MSIZE - 1; i++) {
        for (int j = 0; j < MSIZE; j++) {
            if (M[i][j] == 1)
                UFUnion(uf, i, j);
        }
    }
    printf("Should be 2, resl = %d\n", UFCount(uf));
    UFDestroy(uf);
    return 0;
}