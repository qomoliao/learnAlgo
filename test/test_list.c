#include <stdio.h>
#include "list.h"


void print_list(List *list)
{
    ListElmt *list_elmt;
    int i;

    list_elmt = list_head(list);
    printf("List:");
    for (i = 0; i < list_size(list); i++) {
        printf(" %d", *(int *)list_data(list_elmt));
        list_elmt = list_next(list_elmt);
    }
    printf("\n");
}

/*
 * 删除指定节点
 */
void test_del_node(int *list_dats, int num_datas, int del_node)
{
    List list;
    ListElmt *list_elmt;
    int *del_node_ptr;
    int i;

    printf("\ntest_del_node.\nlist init\n");
    list_init(&list, NULL);
    for (i = 0; i < num_datas; i++)
        list_ins_next(&list, list_tail(&list), &list_dats[i]);
    print_list(&list);

    printf("list remove node %d\n", del_node);
    list_elmt = list_head(&list);
    if (*(int *)list_data(list_elmt) == del_node) {
        list_rem_next(&list, NULL, (void **)&del_node_ptr);
    } else {
        while (!list_is_tail(list_elmt))
        {
            if (*(int *)list_data(list_next(list_elmt)) == del_node)
            {
                list_rem_next(&list, list_elmt, (void **)&del_node_ptr);
                break;
            }
            list_elmt = list_next(list_elmt);
        }
    }
    print_list(&list);
    printf("del node is %d\n", *del_node_ptr);
    list_destroy(&list);
}

/* 
 * 删除链表的倒数第N个节点
 */
void test_del_node_by_reverse_index(int *list_dats, int num_datas, int re_idx)
{
    List list;
    ListElmt *list_elmt;
    int *del_node_ptr;
    int i;

    printf("\ntest_del_node_by_reverse_index.\nlist init\n");
    list_init(&list, NULL);
    for (i = 0; i < num_datas; i++)
        list_ins_next(&list, list_tail(&list), &list_dats[i]);
    print_list(&list);

    printf("list remove node in reverse index: %d\n", re_idx);
    list_elmt = list_head(&list);
    if (re_idx == num_datas) {
        list_rem_next(&list, NULL, (void **)&del_node_ptr);
    } else {
        for (i = 0; i < list_size(&list); i++)
        {
            if (list_size(&list) - re_idx == i + 1)
            {
                list_rem_next(&list, list_elmt, (void **)&del_node_ptr);
                break;
            }
            list_elmt = list_next(list_elmt);
        }
    }
    print_list(&list);
    printf("del node is %d\n", *del_node_ptr);
    list_destroy(&list);
}

/*
 * 合并两个有序链表，成一个有序链表
 */
void test_merge_two_list(int *dats0, int num_dats0, int *dats1, int num_dats1)
{
    List list_merge, list_0, list_1;
    ListElmt *next_elem_0, *next_elem_1, *merge_elem;
    int i;

    printf("\ntest_merge_two_list.\nlist init\n");
    list_init(&list_merge, NULL);
    list_init(&list_0, NULL);
    for (i = 0; i < num_dats0; i++)
        list_ins_next(&list_0, list_tail(&list_0), &dats0[i]);
    printf("List 0 >> ");
    print_list(&list_0);
    list_init(&list_1, NULL);
    for (i = 0; i < num_dats1; i++)
        list_ins_next(&list_1, list_tail(&list_1), &dats1[i]);
    printf("List 1 >> ");
    print_list(&list_1);
    
    printf("Merge List >> ");
    next_elem_0 = list_head(&list_0);
    next_elem_1 = list_head(&list_1);
    while (next_elem_0 != NULL || next_elem_1 != NULL) {
        if (next_elem_0 == NULL) {
            list_ins_next(&list_merge, list_tail(&list_merge), list_data(next_elem_1));
            next_elem_1 = list_next(next_elem_1);
        } else if (next_elem_1 == NULL) {
            list_ins_next(&list_merge, list_tail(&list_merge), list_data(next_elem_0));
            next_elem_0 = list_next(next_elem_0);
        } else {
            if (*(int *)list_data(next_elem_0) <= *(int *)list_data(next_elem_1)) {
                list_ins_next(&list_merge, list_tail(&list_merge), list_data(next_elem_0));
                next_elem_0 = list_next(next_elem_0);
            } else {
                list_ins_next(&list_merge, list_tail(&list_merge), list_data(next_elem_1));
                next_elem_1 = list_next(next_elem_1);
            }
        }
    }
    print_list(&list_merge);

    list_destroy(&list_merge);
    list_destroy(&list_0);
    list_destroy(&list_1);
}

void main(void)
{
    printf("list testing...\n");
    int dats[4] = {4, 5, 1, 9};

    test_del_node(dats, 4, 5);
    test_del_node(dats, 4, 1);
    test_del_node(dats, 4, 4);
    test_del_node(dats, 4, 9);

    int dats1[5] = {1, 2, 3, 4, 5};
    test_del_node_by_reverse_index(dats1, 5, 2);
    test_del_node_by_reverse_index(dats1, 5, 1);
    test_del_node_by_reverse_index(dats1, 5, 3);
    test_del_node_by_reverse_index(dats1, 5, 4);
    test_del_node_by_reverse_index(dats1, 5, 5);

    int dats2[3] = {1, 2, 4};
    int dats3[3] = {1, 3, 4};
    test_merge_two_list(dats2, 3, dats3, 3);
}