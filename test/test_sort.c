#include <stdio.h>
#include "sort.h"

void printInts(int *datas, int nums)
{
    int i;

    for (i = 0; i < nums; i++)
        printf("%d, ", datas[i]);
    printf("\n");
}

int intCompare(const void *key1, const void *key2)
{
    int ret;

    if (*(int*)key1 > *(int*)key2)
        ret = 1;
    else if (*(int*)key1 < *(int*)key2)
        ret = -1;
    else
        ret = 0;
    return ret;
}

int main(void)
{
    int data[10] = {34, 83, 23, 1, 43, 55, 23, 3, 1, 10};

    printf("Unsort datas: ");
    printInts(data, 10);
    issort(data, 10, sizeof(int), intCompare);
    printf("Issort datas: ");
    printInts(data, 10);

    return 0;
}