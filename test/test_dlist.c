#include <stdio.h>
#include "dlist.h"

void print_dlist(DList *list)
{
    DListElmt *list_elmt;
    int i;

    list_elmt = dlist_head(list);
    printf("DList:");
    for (i = 0; i < dlist_size(list); i++) {
        printf(" %d", *(int *)dlist_data(list_elmt));
        list_elmt = dlist_next(list_elmt);
    }
    printf("\n");
}

/*
 * 反转链表
 */
void test_list_reverse(int *dats, int num_dat)
{
    DList list, list_rev;
    DListElmt *dlist_elmt;
    int i, ret;

    printf("\ntest_list_reverse\ndlist init\n");
    dlist_init(&list, NULL);
    dlist_init(&list_rev, NULL);

    ret = dlist_ins_next(&list, NULL, &dats[0]);
    if (ret)
        printf("dlist_ins_next err: %d\n", ret);
    for (i = 1; i < num_dat; i++)
        dlist_ins_next(&list, dlist_tail(&list), &dats[i]);
    print_dlist(&list);

    printf("Reverse list:\n");
    if (dlist_size(&list) == 0)
        return;
    dlist_elmt = dlist_tail(&list);
    dlist_ins_next(&list_rev, NULL, dlist_data(dlist_elmt));
    dlist_elmt = dlist_prev(dlist_elmt);
    while (dlist_elmt != NULL) {
        dlist_ins_next(&list_rev, dlist_tail(&list_rev), dlist_data(dlist_elmt));
        dlist_elmt = dlist_prev(dlist_elmt);
    }
    print_dlist(&list_rev);

    dlist_destroy(&list);
    dlist_destroy(&list_rev);
}

/* 
 * 回文链表
 */
void test_palindrome(int *dats, int num_dat)
{
    DList list;
    DListElmt *elmt_from_head, *elmt_from_tail;
    int i, ret;

    ret = 1;
    printf("\ntest_palindrome\ndlist init\n");
    dlist_init(&list, NULL);
    for (i = 0; i < num_dat; i++)
        dlist_ins_next(&list, dlist_tail(&list), &dats[i]);
    print_dlist(&list);

    printf("Check palindrome: ");
    elmt_from_head = dlist_head(&list);
    elmt_from_tail = dlist_tail(&list);
    if (elmt_from_head == NULL)
    {
        printf("Is NULL list\n");
    }
    else
    {
        while (elmt_from_head != NULL)
        {
            if (*(int *)dlist_data(elmt_from_head) != *(int *)dlist_data(elmt_from_tail)) {
                ret = 0;
                break;
            }
            elmt_from_head = dlist_next(elmt_from_head);
            elmt_from_tail = dlist_prev(elmt_from_tail);
        }
    }
    printf("%d\n", ret);

    dlist_destroy(&list);
}

void main(void)
{
    int dat0[5] = {1, 2, 3, 4, 5};

    printf("\ndlist testing...\n");
    test_list_reverse(dat0, 5);

    test_palindrome(dat0, 5);
    int dat1[5] = {1, 2, 3, 2, 1};
    test_palindrome(dat1, 5);
}