#ifndef H_SORT
#define H_SORT

int issort(void *data, int size, int esize, int (*compare)(const void *key1, const void *key2));
#endif