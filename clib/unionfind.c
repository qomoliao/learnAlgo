#include "unionfind.h"

UnionFind *UFInit(int size)
{
    UnionFind *uf;

    uf = malloc(sizeof(UnionFind));
    uf->count = size;

    uf->parent = malloc(sizeof(int) * size);
    uf->size = malloc(sizeof(int) * size);
    for (int i = 0; i < size; i++) {
        uf->parent[i] = i;
        uf->size[i] = 1;
    }

    return uf;
}

int UFCount(UnionFind *uf)
{
    return uf->count;
}

int UFFind(UnionFind *uf, int x)
{
    while (x != uf->parent[x]) {
        uf->parent[x] = uf->parent[uf->parent[x]];
        x = uf->parent[x];
    }
    return x;
}

int UFConneted(UnionFind *uf, int x, int y)
{
    if (UFFind(uf, x) != UFFind(uf, y))
        return 0;
    return 1;
}

int UFUnion(UnionFind *uf, int x, int y)
{
    int rootX, rootY;

    if (UFConneted(uf, x, y) == 1)
        return 0;
    rootX = UFFind(uf, x);
    rootY = UFFind(uf, y);
    if (uf->size[rootX] > uf->size[rootY]) {
        uf->parent[rootY] = uf->parent[rootX];
        uf->size[rootX] += uf->size[rootY];
    } else {
        uf->parent[rootX] = uf->parent[rootY];
        uf->size[rootY] += uf->size[rootX];
    }
    uf->count--;
    return 0;
}

int UFDestroy(UnionFind *uf)
{
    if (uf->parent != NULL)
        free(uf->parent);
    if (uf->size != NULL)
        free(uf->size);
    free(uf);
    return 0;
}