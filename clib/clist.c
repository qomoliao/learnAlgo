#include "clist.h"


void clist_init(CList *list, void (*destroy)(void *data))
{
    list->size = 0;
    list->head = NULL;
    list->destroy = destroy;
    return;
}

void clist_destroy(CList *list)
{
    void *data;

    while (clist_size(list) > 0)
    {
        if (clist_rem_next(list, clist_head(list), (void **)&data) == 0 &&
            list->destroy != NULL)
            list->destroy(data);
    }
    memset(list, 0, sizeof(CList));
    return;
}

int clist_ins_next(CList *list, CListElmt *element, const void *data)
{
    CListElmt *new_elemt;

    if (element == NULL && clist_size(list) != 0)
        return -1;
    
    if ((new_elemt = (CListElmt *)malloc(sizeof(CListElmt))) == NULL)
        return -1;

    new_elemt->data = data;
    if (clist_size(list) == 0)
    {
        list->head = new_elemt;
        new_elemt->next = list->head;
    } else {
        new_elemt->next = element->next;
        element->next = new_elemt;
    }

    list->size++;
    return 0;
}

int clist_rem_next(CList *list, CListElmt *element, void **data)
{
    CListElmt *del_elemt;

    if (clist_size(list) == 0)
        return -1;

    del_elemt = element->next;
    *data = clist_data(del_elemt);
    element->next = del_elemt->next;
    if (del_elemt == clist_head(list))
        list->head = del_elemt->next;

    list->size--;
    if (clist_size(list) == 0)
        list->head = NULL;

    free(del_elemt);
    return 0;
}