#ifndef UNIONFIND_H
#define UNIONFIND_H
#include "stddef.h"
#include "stdint.h"

typedef struct UnionFind_ {
    int *parent;
    int *size;
    int count;
} UnionFind;

UnionFind *UFInit(int size);
int UFCount(UnionFind *uf);
int UFFind(UnionFind *uf, int x);
int UFConneted(UnionFind *uf, int x, int y);
int UFUnion(UnionFind *uf, int x, int y);
int UFDestroy(UnionFind *uf);

#endif // UNIONFIND_H